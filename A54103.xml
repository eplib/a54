<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A54103">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>William Penn's Ansvver to John Faldo's printed Challenge</title>
    <author>Penn, William, 1644-1718.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A54103 of text R222449 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing P1254A). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A54103</idno>
    <idno type="STC">Wing P1254A</idno>
    <idno type="STC">ESTC R222449</idno>
    <idno type="EEBO-CITATION">99833627</idno>
    <idno type="PROQUEST">99833627</idno>
    <idno type="VID">38105</idno>
    <idno type="PROQUESTGOID">2240926025</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A54103)</note>
    <note>Transcribed from: (Early English Books Online ; image set 38105)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 2175:08)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>William Penn's Ansvver to John Faldo's printed Challenge</title>
      <author>Penn, William, 1644-1718.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>by Andrew Sowle,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1674]</date>
     </publicationStmt>
     <notesStmt>
      <note>Dated at end: London the 12th of the 8th month, 1674.</note>
      <note>Imprint from Wing.</note>
      <note>A reply to an anti-Quaker tract.</note>
      <note>Reproduction of the original in the Cambridge University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Faldo, John, 1633-1690. -- Challenge -- Controversial literature -- Early works to 1800.</term>
     <term>Society of Friends -- Early works to 1800.</term>
     <term>Quakers -- England -- London -- Apologetic works -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>William Penn's Ansvver to John Faldo's printed Challenge.</ep:title>
    <ep:author>Penn, William, </ep:author>
    <ep:publicationYear>1674</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>490</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2005-08</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2005-09</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2005-10</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2005-10</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2006-01</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A54103-t">
  <body xml:id="A54103-e0">
   <div type="text" xml:id="A54103-e10">
    <pb facs="tcp:38105:1" rend="simple:additions" xml:id="A54103-001-a"/>
    <head xml:id="A54103-e20">
     <w lemma="William" pos="nn1" xml:id="A54103-001-a-0010">William</w>
     <w lemma="penn" pos="nng1" xml:id="A54103-001-a-0020">Penn's</w>
     <w lemma="answer" pos="n1" xml:id="A54103-001-a-0030">ANSWER</w>
     <w lemma="to" pos="acp" xml:id="A54103-001-a-0040">TO</w>
     <w lemma="JOHN" pos="nn1" xml:id="A54103-001-a-0050">JOHN</w>
     <w lemma="faldo" pos="nng1" xml:id="A54103-001-a-0060">FALDO's</w>
     <w lemma="print" pos="vvn" xml:id="A54103-001-a-0070">PRINTED</w>
     <w lemma="challenge" pos="n1" xml:id="A54103-001-a-0080">CHALLENGE</w>
     <pc unit="sentence" xml:id="A54103-001-a-0090">.</pc>
    </head>
    <p xml:id="A54103-e30">
     <w lemma="it" pos="pn" xml:id="A54103-001-a-0100">IT</w>
     <w lemma="will" pos="vmd" xml:id="A54103-001-a-0110">would</w>
     <w lemma="seem" pos="vvi" xml:id="A54103-001-a-0120">seem</w>
     <hi xml:id="A54103-e40">
      <w lemma="strange" pos="j" xml:id="A54103-001-a-0130">Strange</w>
      <pc xml:id="A54103-001-a-0140">,</pc>
     </hi>
     <w lemma="as" pos="acp" xml:id="A54103-001-a-0150">as</w>
     <w lemma="well" pos="av" xml:id="A54103-001-a-0160">well</w>
     <w lemma="as" pos="acp" xml:id="A54103-001-a-0170">as</w>
     <w lemma="unreasonable" pos="j" rend="hi" xml:id="A54103-001-a-0180">Unreasonable</w>
     <w lemma="to" pos="acp" xml:id="A54103-001-a-0190">to</w>
     <w lemma="i" pos="pno" xml:id="A54103-001-a-0200">me</w>
     <pc xml:id="A54103-001-a-0210">,</pc>
     <w lemma="that" pos="cs" xml:id="A54103-001-a-0220">that</w>
     <hi xml:id="A54103-e60">
      <w lemma="j." pos="ab" xml:id="A54103-001-a-0230">J.</w>
      <w lemma="Faldo" pos="nn1" xml:id="A54103-001-a-0240">Faldo</w>
     </hi>
     <w lemma="shall" pos="vmd" xml:id="A54103-001-a-0250">should</w>
     <w lemma="of" pos="acp" xml:id="A54103-001-a-0260">of</w>
     <w lemma="all" pos="d" xml:id="A54103-001-a-0270">all</w>
     <w lemma="time" pos="n2" xml:id="A54103-001-a-0280">Times</w>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-0290">and</w>
     <w lemma="place" pos="n2" xml:id="A54103-001-a-0300">Places</w>
     <w lemma="choose" pos="vvi" reg="choose" xml:id="A54103-001-a-0310">chuse</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-0320">the</w>
     <w lemma="Barbican" pos="nn1" rend="hi" xml:id="A54103-001-a-0330">Barbican</w>
     <w lemma="meet" pos="vvg" xml:id="A54103-001-a-0340">Meeting</w>
     <w lemma="of" pos="acp" xml:id="A54103-001-a-0350">of</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-0360">the</w>
     <w lemma="28" pos="ord" rend="hi" xml:id="A54103-001-a-0370">28th</w>
     <w lemma="of" pos="acp" xml:id="A54103-001-a-0380">of</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-0390">the</w>
     <w lemma="6" pos="ord" rend="hi" xml:id="A54103-001-a-0400">6th</w>
     <w lemma="month" pos="n1" xml:id="A54103-001-a-0410">Month</w>
     <pc xml:id="A54103-001-a-0420">,</pc>
     <w lemma="so" pos="av" xml:id="A54103-001-a-0430">so</w>
     <w lemma="surreptitious" pos="av-j" xml:id="A54103-001-a-0440">surreptitiously</w>
     <w lemma="get" pos="vvn" xml:id="A54103-001-a-0450">gotten</w>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-0460">and</w>
     <w lemma="partial" pos="av-j" xml:id="A54103-001-a-0470">partially</w>
     <w lemma="manage" pos="vvn" reg="managed" xml:id="A54103-001-a-0480">mannaged</w>
     <pc xml:id="A54103-001-a-0490">,</pc>
     <w lemma="to" pos="prt" xml:id="A54103-001-a-0500">to</w>
     <w lemma="divulge" pos="vvi" xml:id="A54103-001-a-0510">divulge</w>
     <w lemma="his" pos="po" xml:id="A54103-001-a-0520">his</w>
     <w lemma="challenge" pos="n1" rend="hi" xml:id="A54103-001-a-0530">Challenge</w>
     <w lemma="against" pos="acp" xml:id="A54103-001-a-0540">against</w>
     <w lemma="i" pos="pno" xml:id="A54103-001-a-0550">me</w>
     <pc xml:id="A54103-001-a-0560">,</pc>
     <w lemma="do" pos="vvd" xml:id="A54103-001-a-0570">did</w>
     <w lemma="I" pos="pns" xml:id="A54103-001-a-0580">I</w>
     <w lemma="not" pos="xx" xml:id="A54103-001-a-0590">not</w>
     <w lemma="give" pos="vvi" xml:id="A54103-001-a-0600">give</w>
     <w lemma="off" pos="acp" xml:id="A54103-001-a-0610">off</w>
     <w lemma="wonder" pos="vvg" rend="hi" xml:id="A54103-001-a-0620">Wondering</w>
     <w lemma="at" pos="acp" xml:id="A54103-001-a-0630">at</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-0640">the</w>
     <hi xml:id="A54103-e120">
      <w lemma="injust" pos="j" xml:id="A54103-001-a-0650">Injust</w>
      <w lemma="carriage" pos="n1" xml:id="A54103-001-a-0660">Carriage</w>
     </hi>
     <w lemma="of" pos="acp" xml:id="A54103-001-a-0670">of</w>
     <w lemma="some" pos="d" xml:id="A54103-001-a-0680">some</w>
     <w lemma="man" pos="n2" xml:id="A54103-001-a-0690">men</w>
     <w lemma="towards" pos="acp" xml:id="A54103-001-a-0700">towards</w>
     <w lemma="we" pos="pno" xml:id="A54103-001-a-0710">us</w>
     <pc unit="sentence" xml:id="A54103-001-a-0720">.</pc>
    </p>
    <p xml:id="A54103-e130">
     <w lemma="for" pos="acp" xml:id="A54103-001-a-0730">For</w>
     <hi xml:id="A54103-e140">
      <w lemma="first" pos="ord" xml:id="A54103-001-a-0740">first</w>
      <pc xml:id="A54103-001-a-0750">,</pc>
     </hi>
     <w lemma="he" pos="pns" xml:id="A54103-001-a-0760">He</w>
     <w lemma="can" pos="vmd" xml:id="A54103-001-a-0770">could</w>
     <w lemma="not" pos="xx" xml:id="A54103-001-a-0780">not</w>
     <w lemma="but" pos="acp" xml:id="A54103-001-a-0790">but</w>
     <w lemma="know" pos="vvb" xml:id="A54103-001-a-0800">know</w>
     <pc xml:id="A54103-001-a-0810">,</pc>
     <w lemma="I" pos="pns" xml:id="A54103-001-a-0820">I</w>
     <w lemma="be" pos="vvd" xml:id="A54103-001-a-0830">was</w>
     <w lemma="at" pos="acp" xml:id="A54103-001-a-0840">at</w>
     <w lemma="a" pos="d" xml:id="A54103-001-a-0850">a</w>
     <hi xml:id="A54103-e150">
      <w lemma="great" pos="j" xml:id="A54103-001-a-0860">great</w>
      <w lemma="distance" pos="n1" xml:id="A54103-001-a-0870">Distance</w>
     </hi>
     <w lemma="from" pos="acp" xml:id="A54103-001-a-0880">from</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-0890">the</w>
     <w lemma="place" pos="n1" xml:id="A54103-001-a-0900">Place</w>
     <pc xml:id="A54103-001-a-0910">;</pc>
     <w lemma="that" pos="cs" xml:id="A54103-001-a-0920">that</w>
     <w lemma="I" pos="pns" xml:id="A54103-001-a-0930">I</w>
     <w lemma="have" pos="vvd" xml:id="A54103-001-a-0940">had</w>
     <w lemma="twice" pos="av" xml:id="A54103-001-a-0950">twice</w>
     <hi xml:id="A54103-e160">
      <w lemma="defend" pos="vvn" xml:id="A54103-001-a-0960">defended</w>
      <w lemma="our" pos="po" xml:id="A54103-001-a-0970">our</w>
      <w lemma="belief" pos="n1" xml:id="A54103-001-a-0980">Belief</w>
     </hi>
     <w lemma="in" pos="acp" xml:id="A54103-001-a-0990">in</w>
     <w lemma="print" pos="n1" xml:id="A54103-001-a-1000">Print</w>
     <w lemma="against" pos="acp" xml:id="A54103-001-a-1010">against</w>
     <w lemma="he" pos="pno" xml:id="A54103-001-a-1020">him</w>
     <pc xml:id="A54103-001-a-1030">;</pc>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-1040">and</w>
     <w lemma="that" pos="cs" xml:id="A54103-001-a-1050">that</w>
     <w lemma="a" pos="d" xml:id="A54103-001-a-1060">a</w>
     <hi xml:id="A54103-e170">
      <w lemma="considerable" pos="j" xml:id="A54103-001-a-1070">considerable</w>
      <w lemma="book" pos="n1" xml:id="A54103-001-a-1080">Book</w>
     </hi>
     <w lemma="lie" pos="vvz" xml:id="A54103-001-a-1090">lieth</w>
     <w lemma="at" pos="acp" xml:id="A54103-001-a-1100">at</w>
     <w lemma="his" pos="po" xml:id="A54103-001-a-1110">his</w>
     <w lemma="door" pos="n1" xml:id="A54103-001-a-1120">Door</w>
     <w lemma="unanswered" pos="j" xml:id="A54103-001-a-1130">unanswered</w>
     <pc unit="sentence" xml:id="A54103-001-a-1140">.</pc>
     <w lemma="to" pos="prt" xml:id="A54103-001-a-1150">To</w>
     <w lemma="make" pos="vvi" xml:id="A54103-001-a-1160">make</w>
     <w lemma="then" pos="av" xml:id="A54103-001-a-1170">then</w>
     <w lemma="that" pos="d" xml:id="A54103-001-a-1180">that</w>
     <w lemma="swagger" pos="vvb" rend="hi" xml:id="A54103-001-a-1190">Swagger</w>
     <w lemma="in" pos="acp" xml:id="A54103-001-a-1200">in</w>
     <w lemma="my" pos="po" xml:id="A54103-001-a-1210">my</w>
     <w lemma="absence" pos="n1" xml:id="A54103-001-a-1220">Absence</w>
     <pc xml:id="A54103-001-a-1230">,</pc>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-1240">and</w>
     <w lemma="yet" pos="av" xml:id="A54103-001-a-1250">yet</w>
     <w lemma="be" pos="vvi" xml:id="A54103-001-a-1260">be</w>
     <w lemma="debtor" pos="n1" rend="hi" xml:id="A54103-001-a-1270">Debtor</w>
     <w lemma="to" pos="acp" xml:id="A54103-001-a-1280">to</w>
     <w lemma="my" pos="po" xml:id="A54103-001-a-1290">my</w>
     <w lemma="desence" pos="n2" xml:id="A54103-001-a-1300">Desences</w>
     <pc xml:id="A54103-001-a-1310">;</pc>
     <w lemma="to" pos="prt" xml:id="A54103-001-a-1320">to</w>
     <w lemma="come" pos="vvi" xml:id="A54103-001-a-1330">come</w>
     <w lemma="ten" pos="crd" xml:id="A54103-001-a-1340">ten</w>
     <w lemma="mile" pos="n2" xml:id="A54103-001-a-1350">Miles</w>
     <w lemma="to" pos="prt" xml:id="A54103-001-a-1360">to</w>
     <w lemma="tell" pos="vvi" xml:id="A54103-001-a-1370">tell</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-1380">the</w>
     <w lemma="world" pos="n1" xml:id="A54103-001-a-1390">World</w>
     <pc xml:id="A54103-001-a-1400">,</pc>
     <w lemma="he" pos="pns" xml:id="A54103-001-a-1410">he</w>
     <w lemma="will" pos="vmd" xml:id="A54103-001-a-1420">would</w>
     <w lemma="do" pos="vvi" xml:id="A54103-001-a-1430">do</w>
     <w lemma="great" pos="j" xml:id="A54103-001-a-1440">great</w>
     <w lemma="thing" pos="n2" xml:id="A54103-001-a-1450">Things</w>
     <w lemma="against" pos="acp" xml:id="A54103-001-a-1460">against</w>
     <w lemma="i" pos="pno" xml:id="A54103-001-a-1470">me</w>
     <pc xml:id="A54103-001-a-1480">,</pc>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-1490">and</w>
     <w lemma="when" pos="crq" xml:id="A54103-001-a-1500">when</w>
     <w lemma="he" pos="pns" xml:id="A54103-001-a-1510">he</w>
     <w lemma="have" pos="vvd" xml:id="A54103-001-a-1520">had</w>
     <w lemma="do" pos="vvn" xml:id="A54103-001-a-1530">done</w>
     <pc xml:id="A54103-001-a-1540">,</pc>
     <w lemma="to" pos="prt" xml:id="A54103-001-a-1550">to</w>
     <w lemma="print" pos="vvi" xml:id="A54103-001-a-1560">print</w>
     <pc xml:id="A54103-001-a-1570">,</pc>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-1580">and</w>
     <w lemma="as" pos="acp" xml:id="A54103-001-a-1590">as</w>
     <w lemma="I" pos="pns" xml:id="A54103-001-a-1600">I</w>
     <w lemma="may" pos="vmb" xml:id="A54103-001-a-1610">may</w>
     <w lemma="say" pos="vvi" xml:id="A54103-001-a-1620">say</w>
     <pc xml:id="A54103-001-a-1630">,</pc>
     <w lemma="spit" pos="vvb" xml:id="A54103-001-a-1640">spit</w>
     <w lemma="in" pos="acp" xml:id="A54103-001-a-1650">in</w>
     <w lemma="our" pos="po" xml:id="A54103-001-a-1660">our</w>
     <w lemma="face" pos="n2" xml:id="A54103-001-a-1670">Faces</w>
     <w lemma="at" pos="acp" xml:id="A54103-001-a-1680">at</w>
     <w lemma="our" pos="po" xml:id="A54103-001-a-1690">our</w>
     <w lemma="entrance" pos="n1" xml:id="A54103-001-a-1700">Entrance</w>
     <w lemma="into" pos="acp" xml:id="A54103-001-a-1710">into</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-1720">the</w>
     <w lemma="last" pos="ord" xml:id="A54103-001-a-1730">last</w>
     <w lemma="barbican-meeting" pos="j" rend="plain-hyphen-hi" xml:id="A54103-001-a-1740">Barbican-Meeting</w>
     <pc xml:id="A54103-001-a-1760">,</pc>
     <w lemma="before" pos="acp" xml:id="A54103-001-a-1770">before</w>
     <w lemma="he" pos="pns" xml:id="A54103-001-a-1780">he</w>
     <w lemma="have" pos="vvd" xml:id="A54103-001-a-1790">had</w>
     <w lemma="send" pos="vvn" xml:id="A54103-001-a-1800">sent</w>
     <w lemma="i" pos="pno" xml:id="A54103-001-a-1810">me</w>
     <w lemma="a" pos="d" xml:id="A54103-001-a-1820">a</w>
     <w lemma="copy" pos="n1" xml:id="A54103-001-a-1830">Copy</w>
     <pc xml:id="A54103-001-a-1840">,</pc>
     <w lemma="or" pos="cc" xml:id="A54103-001-a-1850">or</w>
     <w lemma="atleast" pos="av" xml:id="A54103-001-a-1860">atleast</w>
     <w lemma="to" pos="prt" xml:id="A54103-001-a-1870">to</w>
     <w lemma="know" pos="vvi" xml:id="A54103-001-a-1880">know</w>
     <w lemma="of" pos="acp" xml:id="A54103-001-a-1890">of</w>
     <w lemma="my" pos="po" xml:id="A54103-001-a-1900">my</w>
     <w lemma="receipt" pos="n1" xml:id="A54103-001-a-1910">Receipt</w>
     <w lemma="of" pos="acp" xml:id="A54103-001-a-1920">of</w>
     <w lemma="it" pos="pn" xml:id="A54103-001-a-1930">it</w>
     <pc xml:id="A54103-001-a-1940">,</pc>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-1950">and</w>
     <w lemma="answer" pos="vvb" xml:id="A54103-001-a-1960">Answer</w>
     <w lemma="to" pos="acp" xml:id="A54103-001-a-1970">to</w>
     <w lemma="it" pos="pn" xml:id="A54103-001-a-1980">it</w>
     <pc xml:id="A54103-001-a-1990">,</pc>
     <w lemma="who" pos="crq" xml:id="A54103-001-a-2000">who</w>
     <w lemma="be" pos="vvm" xml:id="A54103-001-a-2010">am</w>
     <w lemma="as" pos="acp" xml:id="A54103-001-a-2020">as</w>
     <w lemma="near" pos="acp" xml:id="A54103-001-a-2030">near</w>
     <w lemma="a" pos="d" xml:id="A54103-001-a-2040">a</w>
     <w lemma="neighbour" pos="n1" xml:id="A54103-001-a-2050">Neighbour</w>
     <w lemma="to" pos="acp" xml:id="A54103-001-a-2060">to</w>
     <w lemma="his" pos="po" xml:id="A54103-001-a-2070">his</w>
     <w lemma="residence" pos="n1" xml:id="A54103-001-a-2080">Residence</w>
     <w lemma="as" pos="acp" xml:id="A54103-001-a-2090">as</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-2100">the</w>
     <w lemma="barbican-meeting" pos="j" rend="plain-hyphen-hi" xml:id="A54103-001-a-2110">Barbican-Meeting</w>
     <pc xml:id="A54103-001-a-2130">,</pc>
     <w lemma="I" pos="pns" xml:id="A54103-001-a-2140">I</w>
     <w lemma="think" pos="vvb" xml:id="A54103-001-a-2150">think</w>
     <w lemma="can" pos="vmbx" xml:id="A54103-001-a-2160">cannot</w>
     <w lemma="be" pos="vvi" xml:id="A54103-001-a-2170">be</w>
     <w lemma="repute" pos="vvn" xml:id="A54103-001-a-2180">reputed</w>
     <w lemma="fair" pos="j" xml:id="A54103-001-a-2190">fair</w>
     <w lemma="by" pos="acp" xml:id="A54103-001-a-2200">by</w>
     <w lemma="indifferent" pos="j" xml:id="A54103-001-a-2210">Indifferent</w>
     <w lemma="person" pos="n2" xml:id="A54103-001-a-2220">Persons</w>
     <pc xml:id="A54103-001-a-2230">:</pc>
     <w lemma="how" pos="crq" xml:id="A54103-001-a-2240">How</w>
     <w lemma="unfair" pos="j" rend="hi" xml:id="A54103-001-a-2250">Unfair</w>
     <w lemma="then" pos="av" xml:id="A54103-001-a-2260">then</w>
     <w lemma="be" pos="vvz" xml:id="A54103-001-a-2270">is</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-2280">the</w>
     <w lemma="complaint" pos="n1" xml:id="A54103-001-a-2290">Complaint</w>
     <pc xml:id="A54103-001-a-2300">,</pc>
     <w lemma="that" pos="cs" xml:id="A54103-001-a-2310">that</w>
     <w lemma="he" pos="pns" xml:id="A54103-001-a-2320">he</w>
     <w lemma="have" pos="vvz" xml:id="A54103-001-a-2330">hath</w>
     <w lemma="have" pos="vvn" xml:id="A54103-001-a-2340">had</w>
     <w lemma="no" pos="dx" xml:id="A54103-001-a-2350">no</w>
     <w lemma="answer" pos="n1" xml:id="A54103-001-a-2360">Answer</w>
     <pc unit="sentence" xml:id="A54103-001-a-2370">?</pc>
    </p>
    <p xml:id="A54103-e230">
     <hi xml:id="A54103-e240">
      <w lemma="again" pos="av" xml:id="A54103-001-a-2380">Again</w>
      <pc xml:id="A54103-001-a-2390">,</pc>
     </hi>
     <w lemma="be" pos="vvd" xml:id="A54103-001-a-2400">Was</w>
     <w lemma="I" pos="pns" xml:id="A54103-001-a-2410">I</w>
     <w lemma="not" pos="xx" xml:id="A54103-001-a-2420">not</w>
     <w lemma="then" pos="av" xml:id="A54103-001-a-2430">then</w>
     <pc xml:id="A54103-001-a-2440">,</pc>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-2450">and</w>
     <w lemma="be" pos="vvm" xml:id="A54103-001-a-2460">am</w>
     <w lemma="I" pos="pns" xml:id="A54103-001-a-2470">I</w>
     <w lemma="not" pos="xx" xml:id="A54103-001-a-2480">not</w>
     <w lemma="still" pos="av" xml:id="A54103-001-a-2490">still</w>
     <w lemma="engage" pos="vvn" xml:id="A54103-001-a-2500">engaged</w>
     <w lemma="against" pos="acp" xml:id="A54103-001-a-2510">against</w>
     <w lemma="other" pos="d" xml:id="A54103-001-a-2520">other</w>
     <w lemma="person" pos="n2" xml:id="A54103-001-a-2530">Persons</w>
     <pc xml:id="A54103-001-a-2540">,</pc>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-2550">and</w>
     <w lemma="that" pos="cs" xml:id="A54103-001-a-2560">that</w>
     <w lemma="most" pos="avs-d" xml:id="A54103-001-a-2570">mostly</w>
     <w lemma="about" pos="acp" xml:id="A54103-001-a-2580">about</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-2590">the</w>
     <w lemma="same" pos="d" xml:id="A54103-001-a-2600">same</w>
     <w lemma="thing" pos="n2" xml:id="A54103-001-a-2610">things</w>
     <pc unit="sentence" xml:id="A54103-001-a-2620">?</pc>
     <w lemma="why" pos="crq" xml:id="A54103-001-a-2630">Why</w>
     <w lemma="do" pos="vvd" xml:id="A54103-001-a-2640">did</w>
     <w lemma="he" pos="pns" xml:id="A54103-001-a-2650">he</w>
     <w lemma="not" pos="xx" xml:id="A54103-001-a-2660">not</w>
     <w lemma="send" pos="vvi" xml:id="A54103-001-a-2670">send</w>
     <w lemma="i" pos="pno" xml:id="A54103-001-a-2680">me</w>
     <w lemma="word" pos="n1" xml:id="A54103-001-a-2690">word</w>
     <w lemma="he" pos="pns" xml:id="A54103-001-a-2700">he</w>
     <w lemma="intend" pos="vvd" xml:id="A54103-001-a-2710">intended</w>
     <w lemma="to" pos="prt" xml:id="A54103-001-a-2720">to</w>
     <w lemma="be" pos="vvi" xml:id="A54103-001-a-2730">be</w>
     <w lemma="there" pos="av" xml:id="A54103-001-a-2740">there</w>
     <pc xml:id="A54103-001-a-2750">,</pc>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-2760">and</w>
     <w lemma="exhibit" pos="vvi" xml:id="A54103-001-a-2770">exhibit</w>
     <w lemma="a" pos="d" xml:id="A54103-001-a-2780">a</w>
     <w lemma="charge" pos="n1" xml:id="A54103-001-a-2790">Charge</w>
     <w lemma="public" pos="av-j" reg="publicly" xml:id="A54103-001-a-2800">publickly</w>
     <w lemma="against" pos="acp" xml:id="A54103-001-a-2810">against</w>
     <w lemma="i" pos="pno" xml:id="A54103-001-a-2820">me</w>
     <pc unit="sentence" xml:id="A54103-001-a-2830">?</pc>
     <w lemma="the" pos="d" xml:id="A54103-001-a-2840">The</w>
     <hi xml:id="A54103-e250">
      <w lemma="english" pos="jnn" xml:id="A54103-001-a-2850">English</w>
      <w lemma="custom" pos="n1" xml:id="A54103-001-a-2860">Custom</w>
     </hi>
     <w lemma="as" pos="acp" xml:id="A54103-001-a-2870">as</w>
     <w lemma="to" pos="prt" xml:id="A54103-001-a-2880">to</w>
     <w lemma="common" pos="j" xml:id="A54103-001-a-2890">Common</w>
     <w lemma="right" pos="n1-j" xml:id="A54103-001-a-2900">Right</w>
     <pc unit="sentence" xml:id="A54103-001-a-2910">.</pc>
     <w lemma="be" pos="vvz" xml:id="A54103-001-a-2920">Is</w>
     <w lemma="this" pos="d" xml:id="A54103-001-a-2930">this</w>
     <w lemma="to" pos="prt" xml:id="A54103-001-a-2940">to</w>
     <w lemma="prove" pos="vvi" xml:id="A54103-001-a-2950">prove</w>
     <hi xml:id="A54103-e260">
      <w lemma="quakerism" pos="n1" xml:id="A54103-001-a-2960">Quakerism</w>
      <w lemma="no" pos="dx" xml:id="A54103-001-a-2970">no</w>
      <w lemma="christianity" pos="nn1" xml:id="A54103-001-a-2980">Christianity</w>
      <pc xml:id="A54103-001-a-2990">;</pc>
     </hi>
     <w lemma="or" pos="cc" xml:id="A54103-001-a-3000">or</w>
     <w lemma="himself" pos="pr" xml:id="A54103-001-a-3010">himself</w>
     <hi xml:id="A54103-e270">
      <w lemma="no" pos="dx" xml:id="A54103-001-a-3020">No</w>
      <w lemma="christian" pos="jnn" xml:id="A54103-001-a-3030">Christian</w>
      <pc unit="sentence" xml:id="A54103-001-a-3040">?</pc>
     </hi>
     <w lemma="I" pos="pns" xml:id="A54103-001-a-3050">I</w>
     <w lemma="can" pos="vmbx" xml:id="A54103-001-a-3060">cannot</w>
     <w lemma="believe" pos="vvi" xml:id="A54103-001-a-3070">believe</w>
     <w lemma="he" pos="pns" xml:id="A54103-001-a-3080">he</w>
     <w lemma="do" pos="vvd" xml:id="A54103-001-a-3090">did</w>
     <w lemma="to" pos="acp" xml:id="A54103-001-a-3100">to</w>
     <w lemma="i" pos="pno" xml:id="A54103-001-a-3110">me</w>
     <w lemma="herein" pos="av" xml:id="A54103-001-a-3120">herein</w>
     <pc xml:id="A54103-001-a-3130">,</pc>
     <w lemma="as" pos="acp" xml:id="A54103-001-a-3140">as</w>
     <w lemma="he" pos="pns" xml:id="A54103-001-a-3150">he</w>
     <w lemma="will" pos="vmd" xml:id="A54103-001-a-3160">would</w>
     <w lemma="be" pos="vvi" xml:id="A54103-001-a-3170">be</w>
     <w lemma="do" pos="vvn" xml:id="A54103-001-a-3180">done</w>
     <w lemma="by" pos="acp" xml:id="A54103-001-a-3190">by</w>
     <pc unit="sentence" xml:id="A54103-001-a-3200">.</pc>
    </p>
    <p xml:id="A54103-e280">
     <w lemma="but" pos="acp" xml:id="A54103-001-a-3210">But</w>
     <w lemma="that" pos="cs" xml:id="A54103-001-a-3220">that</w>
     <w lemma="I" pos="pns" xml:id="A54103-001-a-3230">I</w>
     <w lemma="may" pos="vmb" xml:id="A54103-001-a-3240">may</w>
     <w lemma="acquit" pos="vvi" xml:id="A54103-001-a-3250">acquit</w>
     <w lemma="myself" pos="pr" reg="myself" xml:id="A54103-001-a-3260">my self</w>
     <w lemma="of" pos="acp" xml:id="A54103-001-a-3280">of</w>
     <w lemma="that" pos="d" xml:id="A54103-001-a-3290">that</w>
     <w lemma="duty" pos="n1" xml:id="A54103-001-a-3300">Duty</w>
     <w lemma="incumbent" pos="j" xml:id="A54103-001-a-3310">incumbent</w>
     <w lemma="on" pos="acp" xml:id="A54103-001-a-3320">on</w>
     <w lemma="i" pos="pno" xml:id="A54103-001-a-3330">me</w>
     <hi xml:id="A54103-e290">
      <w lemma="for" pos="acp" xml:id="A54103-001-a-3340">for</w>
      <w lemma="the" pos="d" xml:id="A54103-001-a-3350">the</w>
      <w lemma="truth" pos="n1" xml:id="A54103-001-a-3360">Truth</w>
      <pc xml:id="A54103-001-a-3370">,</pc>
     </hi>
     <w lemma="I" pos="pns" xml:id="A54103-001-a-3380">I</w>
     <w lemma="do" pos="vvb" xml:id="A54103-001-a-3390">do</w>
     <w lemma="hereby" pos="av" xml:id="A54103-001-a-3400">hereby</w>
     <w lemma="signify" pos="vvi" reg="signify" xml:id="A54103-001-a-3410">signifie</w>
     <pc xml:id="A54103-001-a-3420">,</pc>
     <w lemma="that" pos="cs" xml:id="A54103-001-a-3430">That</w>
     <w lemma="inasmuch" pos="av" xml:id="A54103-001-a-3440">inasmuch</w>
     <w lemma="as" pos="acp" xml:id="A54103-001-a-3450">as</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-3460">the</w>
     <w lemma="controversy" pos="n1" reg="Controversy" xml:id="A54103-001-a-3470">Controversie</w>
     <w lemma="depend" pos="vvg" xml:id="A54103-001-a-3480">depending</w>
     <w lemma="between" pos="acp" xml:id="A54103-001-a-3490">between</w>
     <hi xml:id="A54103-e300">
      <w lemma="t." pos="ab" xml:id="A54103-001-a-3500">T.</w>
      <w lemma="h." pos="ab" xml:id="A54103-001-a-3510">H.</w>
      <w lemma="etc." pos="ab" reg="etc." xml:id="A54103-001-a-3520">&amp;c.</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-3530">and</w>
     <w lemma="we" pos="pno" xml:id="A54103-001-a-3540">us</w>
     <pc xml:id="A54103-001-a-3550">,</pc>
     <w lemma="take" pos="vvz" xml:id="A54103-001-a-3560">takes</w>
     <w lemma="in" pos="acp" xml:id="A54103-001-a-3570">in</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-3580">the</w>
     <w lemma="most" pos="avs-d" xml:id="A54103-001-a-3590">most</w>
     <w lemma="of" pos="acp" xml:id="A54103-001-a-3600">of</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-3610">the</w>
     <w lemma="particular" pos="n2-j" xml:id="A54103-001-a-3620">Particulars</w>
     <w lemma="of" pos="acp" xml:id="A54103-001-a-3630">of</w>
     <w lemma="his" pos="po" xml:id="A54103-001-a-3640">his</w>
     <w lemma="charge" pos="n1" xml:id="A54103-001-a-3650">Charge</w>
     <pc xml:id="A54103-001-a-3660">,</pc>
     <w lemma="we" pos="pns" xml:id="A54103-001-a-3670">we</w>
     <w lemma="free" pos="av-j" xml:id="A54103-001-a-3680">freely</w>
     <w lemma="consent" pos="vvi" xml:id="A54103-001-a-3690">consent</w>
     <pc xml:id="A54103-001-a-3700">,</pc>
     <w lemma="that" pos="cs" xml:id="A54103-001-a-3710">that</w>
     <w lemma="he" pos="pns" xml:id="A54103-001-a-3720">he</w>
     <w lemma="shall" pos="vmd" xml:id="A54103-001-a-3730">should</w>
     <w lemma="come" pos="vvi" xml:id="A54103-001-a-3740">come</w>
     <w lemma="in" pos="acp" xml:id="A54103-001-a-3750">in</w>
     <w lemma="with" pos="acp" xml:id="A54103-001-a-3760">with</w>
     <w lemma="they" pos="pno" xml:id="A54103-001-a-3770">them</w>
     <w lemma="for" pos="acp" xml:id="A54103-001-a-3780">for</w>
     <w lemma="a" pos="d" xml:id="A54103-001-a-3790">a</w>
     <w lemma="share" pos="n1" xml:id="A54103-001-a-3800">Share</w>
     <w lemma="as" pos="acp" xml:id="A54103-001-a-3810">as</w>
     <w lemma="confederate" pos="j" rend="hi" xml:id="A54103-001-a-3820">Confederate</w>
     <w lemma="in" pos="acp" xml:id="A54103-001-a-3830">in</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-3840">the</w>
     <w lemma="same" pos="d" xml:id="A54103-001-a-3850">same</w>
     <w lemma="work" pos="n1" xml:id="A54103-001-a-3860">Work</w>
     <pc xml:id="A54103-001-a-3870">,</pc>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-3880">and</w>
     <w lemma="use" pos="vvi" xml:id="A54103-001-a-3890">use</w>
     <w lemma="his" pos="po" xml:id="A54103-001-a-3900">his</w>
     <w lemma="utmost" pos="j" xml:id="A54103-001-a-3910">utmost</w>
     <w lemma="ability" pos="n2" xml:id="A54103-001-a-3920">Abilities</w>
     <w lemma="to" pos="prt" xml:id="A54103-001-a-3930">to</w>
     <w lemma="maintain" pos="vvi" xml:id="A54103-001-a-3940">maintain</w>
     <w lemma="his" pos="po" xml:id="A54103-001-a-3950">his</w>
     <w lemma="accusation" pos="n2" xml:id="A54103-001-a-3960">Accusations</w>
     <pc xml:id="A54103-001-a-3970">;</pc>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-3980">And</w>
     <w lemma="if" pos="cs" xml:id="A54103-001-a-3990">if</w>
     <w lemma="in" pos="acp" xml:id="A54103-001-a-4000">in</w>
     <w lemma="any" pos="d" xml:id="A54103-001-a-4010">any</w>
     <w lemma="thing" pos="n1" xml:id="A54103-001-a-4020">Thing</w>
     <w lemma="his" pos="po" xml:id="A54103-001-a-4030">his</w>
     <w lemma="charge" pos="n1" xml:id="A54103-001-a-4040">Charge</w>
     <w lemma="be" pos="vvz" xml:id="A54103-001-a-4050">is</w>
     <w lemma="singular" pos="j" xml:id="A54103-001-a-4060">singular</w>
     <pc xml:id="A54103-001-a-4070">,</pc>
     <w lemma="we" pos="pns" xml:id="A54103-001-a-4080">we</w>
     <w lemma="shall" pos="vmb" xml:id="A54103-001-a-4090">shall</w>
     <w lemma="be" pos="vvi" xml:id="A54103-001-a-4100">be</w>
     <w lemma="ready" pos="j" xml:id="A54103-001-a-4110">ready</w>
     <w lemma="to" pos="prt" xml:id="A54103-001-a-4120">to</w>
     <w lemma="hear" pos="vvi" xml:id="A54103-001-a-4130">hear</w>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-4140">and</w>
     <w lemma="fair" pos="av-j" xml:id="A54103-001-a-4150">fairly</w>
     <w lemma="debate" pos="vvi" xml:id="A54103-001-a-4160">debate</w>
     <w lemma="it" pos="pn" xml:id="A54103-001-a-4170">it</w>
     <w lemma="at" pos="acp" xml:id="A54103-001-a-4180">at</w>
     <w lemma="the" pos="d" xml:id="A54103-001-a-4190">the</w>
     <w lemma="same" pos="d" xml:id="A54103-001-a-4200">same</w>
     <w lemma="meeting" pos="n1" xml:id="A54103-001-a-4210">Meeting</w>
     <w lemma="or" pos="cc" xml:id="A54103-001-a-4220">or</w>
     <w lemma="meeting" pos="n2" xml:id="A54103-001-a-4230">Meetings</w>
     <pc xml:id="A54103-001-a-4240">,</pc>
     <w lemma="to" pos="prt" xml:id="A54103-001-a-4250">to</w>
     <w lemma="avoid" pos="vvi" xml:id="A54103-001-a-4260">avoid</w>
     <w lemma="fresh" pos="j" xml:id="A54103-001-a-4270">fresh</w>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-4280">and</w>
     <w lemma="unnecessary" pos="j" xml:id="A54103-001-a-4290">unnecessary</w>
     <w lemma="contest" pos="vvz" xml:id="A54103-001-a-4300">Contests</w>
     <pc xml:id="A54103-001-a-4310">,</pc>
     <w lemma="as" pos="acp" xml:id="A54103-001-a-4320">as</w>
     <w lemma="much" pos="av-d" xml:id="A54103-001-a-4330">much</w>
     <w lemma="as" pos="acp" xml:id="A54103-001-a-4340">as</w>
     <w lemma="just" pos="av-j" xml:id="A54103-001-a-4350">justly</w>
     <w lemma="may" pos="vmb" xml:id="A54103-001-a-4360">may</w>
     <w lemma="be" pos="vvi" xml:id="A54103-001-a-4370">be</w>
     <pc xml:id="A54103-001-a-4380">:</pc>
     <w lemma="though" pos="cs" xml:id="A54103-001-a-4390">Though</w>
     <w lemma="still" pos="av" xml:id="A54103-001-a-4400">still</w>
     <w lemma="let" pos="vvb" xml:id="A54103-001-a-4410">let</w>
     <w lemma="it" pos="pn" xml:id="A54103-001-a-4420">it</w>
     <w lemma="be" pos="vvi" xml:id="A54103-001-a-4430">be</w>
     <w lemma="remember" pos="vvn" reg="remembered" xml:id="A54103-001-a-4440">remembred</w>
     <pc xml:id="A54103-001-a-4450">,</pc>
     <w lemma="these" pos="d" xml:id="A54103-001-a-4460">these</w>
     <w lemma="very" pos="j" xml:id="A54103-001-a-4470">very</w>
     <w lemma="thing" pos="n2" xml:id="A54103-001-a-4480">Things</w>
     <w lemma="be" pos="vvb" xml:id="A54103-001-a-4490">are</w>
     <w lemma="at" pos="acp" xml:id="A54103-001-a-4500">at</w>
     <w lemma="large" pos="j" xml:id="A54103-001-a-4510">large</w>
     <w lemma="discourse" pos="vvn" xml:id="A54103-001-a-4520">discoursed</w>
     <w lemma="in" pos="acp" xml:id="A54103-001-a-4530">in</w>
     <w lemma="my" pos="po" xml:id="A54103-001-a-4540">my</w>
     <w lemma="rejoinder" pos="n1" reg="rejoinder" rend="hi" xml:id="A54103-001-a-4550">Rejoynder</w>
     <w lemma="to" pos="acp" xml:id="A54103-001-a-4560">to</w>
     <w lemma="he" pos="pno" xml:id="A54103-001-a-4570">him</w>
     <pc xml:id="A54103-001-a-4580">;</pc>
     <w lemma="our" pos="po" xml:id="A54103-001-a-4590">our</w>
     <w lemma="faith" pos="n1" rend="hi" xml:id="A54103-001-a-4600">Faith</w>
     <w lemma="relieve" pos="vvn" xml:id="A54103-001-a-4610">relieved</w>
     <w lemma="from" pos="acp" xml:id="A54103-001-a-4620">from</w>
     <w lemma="his" pos="po" xml:id="A54103-001-a-4630">his</w>
     <w lemma="unworthy" pos="j" xml:id="A54103-001-a-4640">Unworthy</w>
     <hi xml:id="A54103-e340">
      <w lemma="misrepresentation" pos="n2" xml:id="A54103-001-a-4650">Misrepresentations</w>
      <pc xml:id="A54103-001-a-4660">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-4670">and</w>
     <w lemma="confirm" pos="vvn" xml:id="A54103-001-a-4680">confirmed</w>
     <w lemma="by" pos="acp" xml:id="A54103-001-a-4690">by</w>
     <hi xml:id="A54103-e350">
      <w lemma="scripture" pos="n1" xml:id="A54103-001-a-4700">Scripture</w>
      <pc xml:id="A54103-001-a-4710">,</pc>
      <w lemma="reason" pos="n1" xml:id="A54103-001-a-4720">Reason</w>
      <pc xml:id="A54103-001-a-4730">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-4740">and</w>
     <w lemma="a" pos="d" xml:id="A54103-001-a-4750">a</w>
     <w lemma="cloud" pos="n1" xml:id="A54103-001-a-4760">Cloud</w>
     <w lemma="of" pos="acp" xml:id="A54103-001-a-4770">of</w>
     <w lemma="honourable" pos="j" xml:id="A54103-001-a-4780">Honourable</w>
     <w lemma="ancient" pos="n1" xml:id="A54103-001-a-4790">Ancient</w>
     <w lemma="and" pos="cc" xml:id="A54103-001-a-4800">and</w>
     <w lemma="modern" pos="j" xml:id="A54103-001-a-4810">Modern</w>
     <hi xml:id="A54103-e360">
      <w lemma="witness" pos="n2" xml:id="A54103-001-a-4820">Witnesses</w>
      <pc unit="sentence" xml:id="A54103-001-a-4830">.</pc>
     </hi>
    </p>
    <closer xml:id="A54103-e370">
     <dateline xml:id="A54103-e380">
      <w lemma="London" pos="nn1" xml:id="A54103-001-a-4840">London</w>
      <date xml:id="A54103-e390">
       <w lemma="the" pos="d" rend="hi" xml:id="A54103-001-a-4850">the</w>
       <w lemma="12" pos="ord" xml:id="A54103-001-a-4860">12th</w>
       <hi xml:id="A54103-e410">
        <w lemma="of" pos="acp" xml:id="A54103-001-a-4870">of</w>
        <w lemma="the" pos="d" xml:id="A54103-001-a-4880">the</w>
       </hi>
       <w lemma="8" pos="ord" xml:id="A54103-001-a-4890">8th</w>
       <hi xml:id="A54103-e420">
        <w lemma="month" pos="n1" xml:id="A54103-001-a-4900">Month</w>
        <pc xml:id="A54103-001-a-4910">,</pc>
       </hi>
       <w lemma="1674." pos="crd" xml:id="A54103-001-a-4920">1674.</w>
       <pc unit="sentence" xml:id="A54103-001-a-4930"/>
      </date>
     </dateline>
    </closer>
   </div>
  </body>
  <back xml:id="A54103-t-b"/>
 </text>
</TEI>
